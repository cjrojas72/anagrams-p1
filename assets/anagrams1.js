function getAnagramsOf(input) {
  let results = [];
  let userWord = alphabetize(input);

  for (let i = 0; i < words.length; i++) {
    if (alphabetize(words[i]) === userWord) {
      results.push(words[i]);
    }
  }

  for (stuff in results) {
    const span = document.createElement("span");
    const textContent = document.createTextNode('" ' + results[stuff] + ' ", ');
    span.appendChild(textContent);
    document.getElementById("anagrams-output").appendChild(span);
  }

  return results;
}

function alphabetize(a) {
  return a
    .toLowerCase()
    .split("")
    .sort()
    .join("")
    .trim();
}

const button = document.getElementById("findButton");

button.onclick = function() {
  document.getElementById("anagrams-output").innerHTML = "";
  let typedText = document.getElementById("input").value;
  getAnagramsOf(typedText);
};
